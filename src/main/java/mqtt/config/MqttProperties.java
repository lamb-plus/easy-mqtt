package mqtt.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: Lamb
 * @Date: 2022/10/9 11:57
 */
@ComponentScan(basePackages = {"mqtt.config"})
@Data
@Component
@RefreshScope
@Configuration
public class MqttProperties {

    // MQTT-服务器连接地址，如果有多个，用逗号隔开，如：tcp://192.168.2.68:61613，tcp://192.168.2.69:61613
    @Value("${mqtt.serverUri:tcp://127.0.0.1:1883}")
    private String serverUri;

    // 设置连接的用户名
    @Value("${mqtt.username:admin}")
    private String username;

    // 设置连接的密码
    @Value("${mqtt.password:123456}")
    private String password;

    // 设置是否清空session。
    // false，表示服务器会保留客户端的连接记录；
    // true，表示每次连接到服务器都以新的身份连接。
    private boolean cleanSession = true;

    // 设置连接超时时间，单位为s
    private int connectionTimeout = 30;

    // 设置会话心跳时间，单位为s，服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
    private int keepAliveInterval = 60;

    // 自动重连
    private boolean automaticReconnect = true;

    // 完成超时
    private long completionTimeout = 1000;
    //指定扫描包
    @Value("${mqtt.topicBasePackage:*}")
    private String topicBasePackage;



}
