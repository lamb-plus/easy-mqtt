package mqtt.aware;

import mqtt.receiver.MqttReceiverMessageHandler;
import mqtt.receiver.Receiver;
import mqtt.utils.du;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Author: Lamb
 * @Date: 2022/9/29 11:48
 */
@Component
public class MyApplicationContextAware implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        injection();
    }

    private void injection() {
        MqttReceiverMessageHandler mqttReceiverMessageHandler = applicationContext.getBean(MqttReceiverMessageHandler.class);
        mqttReceiverMessageHandler.setReceiverBeanMap(applicationContext.getBeansOfType(Receiver.class));
        du.printBanner("1.0.0");
    }

}
