package mqtt.utils;

/**
 * @Author: Lamb
 * @Date: 2022/9/23 16:01
 */
public interface CandidateFilter {
    /**
     * 判断是否是候选组件
     *
     * @param className
     * @param c
     * @return
     */
    boolean isCandidate(String className, Class<?> c);


}
