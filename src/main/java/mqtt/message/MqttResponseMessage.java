package mqtt.message;

import com.google.gson.JsonElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Lamb
 * @Date: 2022/9/23 15:49
 */
@Data
@NoArgsConstructor
public class MqttResponseMessage {
    /**
     * 设备serial number ,string类型数字
     */
    private String SN;

    /**
     * 消息类型
     * system：系统消息
     * OTA：升级相关消息
     * application:网关消息（应用消息）
     * network：ZigBee网络相关消息
     */
    private String msgtype;

    /**
     * 消息体，不同的消息，消息体按需定义
     */
    private JsonElement data;

    /**
     * 时间戳：20200513083920
     */
    private String localtime;

    /**
     * 消息ID号。String类型的数字，32个字符，且每个消息ID在当前设备中具有唯一性
     */
    private String msgid;

    /**
     * 消息结果状态：
     * 0：success
     * 1：failure
     * 2：timeout
     */
    private int msgstatus;


//    public static void main(String[] args) {
//        String json = "{\n" +
//                "    \"SN\":\"sn\",\n" +
//                "    \"msgtype\":\"msgtype\",\n" +
//                "    \"data\":{\"commandid\":1,\"parameter\":{\"SN\":\"18181818\",\"SWversion\":\"1.0.0.4\",\"HWversion\":\"1.0.0.1\",\"producttype\":\"zigbeeGW\"}},\n" +
//                "    \"localtime\":\"localtime\",\n" +
//                "    \"msgid\":\"msgid\",\n" +
//                "    \"msgstatus\":0\n" +
//                "}";
//        MqttResponseMessage mqttMessageTemplate1 = GsonUtils.toEntity(json, MqttResponseMessage.class);
//        System.out.println("-> " + mqttMessageTemplate1);
//        System.out.println("-> " + mqttMessageTemplate1.getData().getClass());
//        System.out.println("data -> " + mqttMessageTemplate1.getData());
//     //   System.out.println("data -> " + GsonUtils.toEntity(mqttMessageTemplate1.getData(), InitReceiver.GatewayOnlineEntity.class));
//
//        String route = "hello";
//        if (!route.startsWith("/")) {
//            route += "/";
//        }
//        System.out.println("route: " + route);
//    }


}
