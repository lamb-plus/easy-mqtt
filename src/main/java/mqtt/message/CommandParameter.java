package mqtt.message;

import com.google.gson.JsonElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Lamb
 * @Date: 2022/9/23 15:42
 */
@Data
@NoArgsConstructor
public class CommandParameter {
    private int commandid;
    private JsonElement parameter;
}
