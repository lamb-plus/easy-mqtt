package mqtt.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: Lamb
 * @Date: 2022/10/9 15:18
 */
@ComponentScan(basePackages = {"mqtt.config"})
@Data
@Component
@RefreshScope
@Configuration
public class ReceiverProperties {

    @Value("${mqtt.receiver.clientId:test}")
    private String clientId;
    @Value("${mqtt.receiver.qos:1}")
    private int qos;

}
