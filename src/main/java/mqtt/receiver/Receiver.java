package mqtt.receiver;


/**
 * @author xiangqian
 * @date 16:00 2020/05/13
 */
public interface Receiver {

    void handleMessage(MqttMessage mqttMessage);

}
