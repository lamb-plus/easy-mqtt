package mqtt.message;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Lamb
 * @Date: 2022/9/23 15:47
 */
@Data
@NoArgsConstructor
public class MqttRequestMessage<T> {
    /**
     * 设备serial number ,string类型数字
     */
    @JSONField(name = "SN")
    private String SN;

    /**
     * 消息类型
     * system：系统消息
     * OTA：升级相关消息
     * application:网关消息（应用消息）
     * network：ZigBee网络相关消息
     */
    private String msgtype;

    /**
     * 消息体，不同的消息，消息体按需定义
     */
    private T data;

    /**
     * 时间戳：20200513083920
     */
    private String localtime;

    /**
     * 消息ID号。String类型的数字，32个字符，且每个消息ID在当前设备中具有唯一性
     */
    private String msgid;

    /**
     * 消息结果状态：
     * 0：success
     * 1：failure
     * 2：timeout
     */
    private int msgstatus;

    public MqttRequestMessage(String SN, String msgtype, String localtime, String msgid, int msgstatus) {
        super();
        this.SN = SN;
        this.msgtype = msgtype;
        this.localtime = localtime;
        this.msgid = msgid;
        this.msgstatus = msgstatus;
    }


}
