package mqtt.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: Lamb
 * @Date: 2022/10/9 15:18
 */
@ComponentScan(basePackages = {"mqtt.config"})
@Data
@Component
@RefreshScope
@Configuration
public class SenderProperties {
    // clientid即连接MQTT的客户端ID，一般以唯一标识符表示
    @Value("${mqtt.sender.clientId:test}")
    private String clientId;
    @Value("${mqtt.sender.topic:/test2/#}")
    private String topic;
    @Value("${mqtt.sender.qos:1}")
    private int qos;
}
