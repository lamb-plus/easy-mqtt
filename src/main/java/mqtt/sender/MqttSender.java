package mqtt.sender;


import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * MQTT生产者消息发送接口
 */
@Component
@MessagingGateway(defaultRequestChannel = "senderMessageChannel")
public interface MqttSender {

    /**
     * @param topic   主题
     * @param qos     服务质量等级Qos。
     * @param payload 发送的消息内容
     */
    void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos, String payload) throws Exception;

}

