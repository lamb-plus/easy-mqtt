package mqtt.message;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Lamb
 * @Date: 2022/9/23 15:46
 */
@Data
@NoArgsConstructor
public class CommandParameterT<T> {
    private int commandid;
    private T parameter;
}
