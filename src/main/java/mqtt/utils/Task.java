package mqtt.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * 任务
 *
 */
public abstract class Task<T> implements Runnable {

    // 任务ID
    @Getter
    private String id;

    @Setter
    protected T t;

    public Task() {
        this.id = StringUtils.uuid();
    }

    public Task(T t) {
        this.id = StringUtils.uuid();
        this.t = t;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id != null ? id.equals(task.id) : task.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Task{" + "id='" + id + '\'' + '}';
    }
}
