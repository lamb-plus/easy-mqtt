package mqtt.annotation;

import java.lang.annotation.*;

/**
 * 主题注解
 *
 * @Author: Lamb
 * @Date: 2022/9/23 15:23
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Topic {
    // 主题名称
    String name();

}
