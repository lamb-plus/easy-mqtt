package mqtt.receiver;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xiangqian
 * @date 16:07 2020/05/13
 */
@Data
@NoArgsConstructor
public class MqttMessage {

    private String id;
    private String topic;
    private int qos;
    private String payload;

    private boolean retained;

    // 是否重复
    private boolean duplicate;

    private long timestamp;
}
